<?php

use Faker\Generator as Faker;

$factory->define(App\Level::class, function (Faker $faker) {
    return [
        'nivel' => $faker->unique()->randomElement(['Primario', 'Terciario', 'Secundario', 'Universitario', 'Otro'])
    ];
});
