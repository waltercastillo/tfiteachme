<?php

use Faker\Generator as Faker;

$factory->define(App\Rating::class, function (Faker $faker) {
    return [
    	'puntos'	=> $faker->randomFloat(2, 1, 5),
    //	'puntos'	=> $faker->numberBetween($min=1, $max=5),
    	'comentario'=> $faker->sentence,
        'user_id'	=> App\User::all()->random()->id,
    ];
});




