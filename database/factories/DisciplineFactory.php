<?php

use Faker\Generator as Faker;

$factory->define(App\Discipline::class, function (Faker $faker) {
    return [
        'disciplina' => $faker->unique()->randomElement(['Matemáticas', 'Literatura', 'Piano', 'Física', 'Lengua', 'Inglés', 'Algebra'])
    ];
});
