<?php

use Faker\Generator as Faker;

$factory->define(App\Service::class, function (Faker $faker) {
    $fecha = $faker->date($format = 'Y-m-d', $max = 'now');
    return [
    	'estado' 		=> $faker->randomElement(['encurso', 'finalizada']),
    	'fecha_final' 	=> $fecha,
		'fecha_inicio' 	=> $faker->date($format = 'Y-m-d', $max = $fecha),
   	    'user_id' 		=> App\User::all()->random()->id,
		'publication_id'	=> App\Publication::all()->random()->id,        
    ];
});
