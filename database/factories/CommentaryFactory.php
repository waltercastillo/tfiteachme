<?php

use Faker\Generator as Faker;

$factory->define(App\Commentary::class, function (Faker $faker) {
    return [
       

    	'fecha_hora' 		=> $faker->dateTime($max = 'now', $timezone = null),
		'comentario' 		=> $faker->sentence,
    	'user_id' 			=> \App\User::all()->random()->id,
    	'publication_id' 	=> \App\Publication::all()->random()->id,

    ];
});

