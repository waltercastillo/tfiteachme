<?php

use Faker\Generator as Faker;

$factory->define(App\Publication::class, function (Faker $faker) {
    return [
        
			'descripcion' 	=> $faker->sentence,
			'horario' 		=> $faker->randomElement(['mañana','tarde','Noche']),
			'ciudad'		=>$faker->city,
			'provincia'		=>$faker->state,
			'direccion' 	=>$faker->secondaryAddress,
			'lat' 			=>$faker->latitude($min = -90, $max = 90),  
			'lng'			=>$faker->longitude($min = -180, $max = 180),
			'tipo' 			=> $faker->randomElement
								([\App\Publication::FREE,\App\Publication::PREMIUM]),
			'estado' 		=> $faker->randomElement
								([\App\Publication::DESACTIVADA, \App\Publication::ACTIVADA]),
			'discipline_id'	=> App\Discipline::all()->random()->id,
   	        'user_id' 		=> App\User::all()->random()->id,
   	        'level_id' 		=> App\Level::all()->random()->id,	
    ];
});