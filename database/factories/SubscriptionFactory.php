<?php

use Faker\Generator as Faker;

$factory->define(App\Subscription::class, function (Faker $faker) {
    return [
			'nombre'      		=> $faker->unique()->word,
			'precio'			=> $faker->randomFloat(3, 1, 600),
			'tiempo'			=> $faker->randomDigitNotNull,
			'cant_publicacion'	=> $faker->randomDigitNotNull,
			'estado' 			=> $faker->randomElement
			([\App\Subscription::ACTIVADA, \App\Subscription::DESACTIVADA]),

    ];
});