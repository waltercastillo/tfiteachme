<?php
use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    User::truncate();
    User::create([
            'name'     => 'Walter',
            'apellido' => 'Castillo',
            'email'    => 'waltercastillo@live.com.ar',
            'password' => bcrypt('123'),
            'sobremi'  => 'soy walter castillo y vivo en arroyo seco',
            'avatar'   => 'https://lorempixel.com/200/200/people/?88947',
            'ciudad'   =>'Arroyo Seco',
            'provincia'=>'Santa Fe',
            'direccion'=>'San Martin 555',
            'role_id'  => \App\Role::ADMINISTRADOR, 
            'remember_token' => str_random(10),
        ]);
        
      factory(User::class,20)->create();
        
    }
}
