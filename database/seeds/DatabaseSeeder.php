<?php

use App\Commentary;
use App\Discipline;
use App\Level;
use App\Rating;
use App\Service;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
       

        Schema::disableForeignKeyConstraints();//desactiva integridad ref
        Discipline::truncate();
        Level::truncate();
        DB::table('publication_subscription')->truncate();
       
        Service::truncate();
        Commentary::truncate();
        Rating::truncate();
        

        $this->call(RolesTableSeeder::class);
        $this->call(SubscriptionsTableSeeder::class);
         factory(Discipline::class,7)->create();
        $this->call(UsersTableSeeder::class);
         factory(Level::class,5)->create();
        $this->call(PublicationsTableSeeder::class);
         factory(Service::class,40)->create();
         factory(Commentary::class,50)->create();
         factory(Rating::class,50)->create();

        Schema::enableForeignKeyConstraints();//activa integridad ref
    }
}