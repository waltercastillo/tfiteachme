<?php

use App\Subscription;
use Illuminate\Database\Seeder;


class SubscriptionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	Subscription::truncate();
        Subscription::create([
            'nombre'            => 'Básica',
            'precio'            => '200',
            'tiempo'            => '60',
            'cant_publicacion'  => '2',
            'estado'            => Subscription::ACTIVADO

        ]);
        Subscription::create([
            'nombre'            => 'Stándar',
            'precio'            => '300',
            'tiempo'            => '60',
            'cant_publicacion'  => '3',
            'estado'            => Subscription::ACTIVADO

        ]);
        Subscription::create([
            'nombre'            => 'Media',
            'precio'            => '400',
            'tiempo'            => '90',
            'cant_publicacion'  => '5',
            'estado'            =>  Subscription::DESACTIVADO

        ]);
        Subscription::create([
            'nombre'            => 'Premium',
            'precio'            => '500',
            'tiempo'            => '365',
            'cant_publicacion'  => '10',
            'estado'            => Subscription::ACTIVADO

        ]);
    }
}
