<?php


use App\Publication;
use App\Subscription;
use Illuminate\Database\Seeder;

class PublicationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       Publication::truncate();
      factory(Publication::class,10)->create()->each(function(Publication $publication){
          $nombreplan = (Subscription::all()->random())['nombre'];
          $precioplan = (Subscription::all()->random())['precio'];
          $tiempoplan = (Subscription::all()->random())['tiempo'];
          $cant_publicacionplan = (Subscription::all()->random())['cant_publicacion'];
                $publication->subscriptions()->attach(
                        rand(1, 4),
                            [
                                'nombre' => $nombreplan,
                                'precio' => $precioplan,
                                'tiempo' => $tiempoplan, 
                                'cant_publicacion' => $cant_publicacionplan
                            ]);    
        });
    }
}
