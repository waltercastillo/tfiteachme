<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscriptions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre')->unique();
            $table->float('precio', 6, 2);
            $table->float('tiempo', 4, 0);//timepo de duracion en dias
            $table->float('cant_publicacion', 3, 0);//cant de pubic por usuario
            $table->enum('estado', 
                        [\App\Subscription::ACTIVADO, \App\Subscription::DESACTIVADO])
                        ->default(\App\Subscription::ACTIVADO);////ver cuando una publicacion es paga 
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscriptions');
    }
}
