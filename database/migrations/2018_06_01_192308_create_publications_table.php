<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePublicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('publications', function (Blueprint $table) {
            $table->increments('id');

            $table->string('descripcion');
            $table->string('horario');
            $table->string('ciudad');
            $table->string('provincia');
            $table->string('direccion');
            $table->decimal('lat', 17, 13);
            $table->decimal('lng', 17, 13);
            $table->enum('tipo', 
                        [\App\Publication::FREE, \App\Publication::PREMIUM])
                        ->default(\App\Publication::FREE);////ver cuando una publicacion es paga 

            $table->enum('estado', 
                        [\App\Publication::ACTIVADA, \App\Publication::DESACTIVADA])
                        ->default(\App\Publication::ACTIVADA);

            //$table->boolean('estado')->default(true); /// true=publicada -- false=inactiva

            $table->integer('discipline_id')->unsigned();
            $table->foreign('discipline_id')->references('id')->on('disciplines');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('level_id')->unsigned();
            $table->foreign('level_id')->references('id')->on('levels');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('publications');
    }
}
