<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|

*/
Route::view('/inicio', 'inicio');


Route::redirect('/', 'inicio');
Auth::routes();
Route::group(['middleware' => ['auth']], function () {
	Route::get('/home', 'HomeController@index')->name('home');
	
	Route::get('password', 'UserController@password')->name('password'); 					//para editar password
	Route::post('updatepassword', 'UserController@updatePassword')->name('updatepassword'); //para editar password

	route::resource('users', 		'UserController');
	route::resource('disciplines', 	'DisciplineController');
	route::resource('levels', 		'LevelController');
	route::resource('ratings', 		'RatingController');
	route::resource('subscriptions','SubscriptionController');
	route::resource('publications',	'PublicationController');
	route::resource('services',		'ServiceController');
	route::resource('commentaries',	'CommentaryController');
});
