<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Subscription extends Model
{
    use SoftDeletes; 
    const DESACTIVADO   = 'desactivado';
    const ACTIVADO      = 'activado';

    protected $fillable = ['nombre','tiempo', 'cant_publicacion', 'estado', 'precio'];

    protected $dates = ['deleted_at']; 
    public function publications()
    {
        return $this->belongsToMany('App\Publication')->withPivot('nombre', 'precio', 'tiempo', 'cant_publicacion')->withTimestamps();
    }
}
