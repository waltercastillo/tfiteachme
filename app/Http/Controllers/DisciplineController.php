<?php

namespace App\Http\Controllers;

use App\Discipline;
use App\Http\Requests\discipline\DisciplineStoreRequest;
use App\Http\Requests\discipline\DisciplineUpdateRequest;
use Illuminate\Http\Request;

class DisciplineController extends Controller
{
    public function index()
    {
        $disciplines = Discipline::orderBy('id', 'DESC')->paginate(3);
        return view('admin.disciplines.index', compact('disciplines'));
    }

    public function create()
    {
        return view('admin.disciplines.create');
    }

    public function store(DisciplineStoreRequest $request)
    {
        $discipline = Discipline::create($request->all());
        return redirect('disciplines')->with('info', 'Disciplina creada con éxito');      
    }

    public function edit(Discipline $discipline)
    {
        return view('admin.disciplines.edit', compact('discipline'));
    }

    public function update(DisciplineUpdateRequest $request, Discipline $discipline)
    {
        $discipline->fill($request->all())->save();
        return redirect('disciplines')->with('info', 'Disciplina actualizada con éxito');
    }

    public function destroy(Discipline $discipline)
    {
        $discipline->delete();
        return back()->with('info', 'Eliminado correctamente');
    }
}
