<?php

namespace App\Http\Controllers;

use App\Commentary;
use Illuminate\Http\Request;
use App\Http\Requests\commentary\CommentaryUpdateRequest;
use App\Http\Requests\commentary\CommentaryStoreRequest;

class CommentaryController extends Controller
{
       public function index()
    {
        $commentaries  = Commentary::with('user')->orderBy('id', 'DESC')->paginate(10); 

 
/*        $commentaries = Commentary::orderBy('id', 'DESC')->paginate(10);
*/        return view('admin.commentaries.index', compact('commentaries'));
    }

    public function show(Commentary $commentary)
    {
        return view('admin.commentaries.show', compact('commentary'));
    }

/*    public function create()
    {
        $disciplines = Discipline::orderBy('disciplina', 'ASC')->pluck('disciplina', 'id');
        $levels = Level::orderBy('nivel', 'ASC')->pluck('nivel', 'id');
        return view('admin.commentaries.create', compact('disciplines', 'levels'));  
    }

    public function store(CommentaryStoreRequest $request)
    {
        $commentary = Commentary::create($request->all());
        return redirect('commentaries')->with('info', 'Creada con éxito');      
    }*/

    public function edit(Commentary $commentary)
    {

        return view('admin.commentaries.edit', compact('commentary', 'disciplines', 'levels'));
    }

    public function update(CommentaryUpdateRequest $request, Commentary $commentary)
    {
        $commentary->fill($request->all())->save();
        return redirect('commentaries')->with('info', 'Actualización con éxito');
    }

    public function destroy(Commentary $commentary)
    {
        $commentary->delete();
        return back()->with('info', 'Eliminado correctamente');
    }
}
