<?php

namespace App\Http\Controllers;
use App\Http\Requests\rating\RatingStoreRequest;
use App\Http\Requests\rating\RatingUpdateRequest;
use App\Rating;
use Illuminate\Http\Request;

class RatingController extends Controller
{
    public function index()
    {
        $ratings  = Rating::with('user')->orderBy('user_id', 'ASC')->paginate(10); 

/*        $ratings = Rating::orderBy('user_id', 'asc')->paginate(10);
*/        return view('admin.ratings.index', compact('ratings'));
    }
    
    public function show(Rating $rating)
    {
        return view('admin.ratings.show', compact('rating'));
    }

    public function edit(Rating $rating)
    {
        return view('admin.ratings.edit', compact('rating'));
    }

    public function update(RatingUpdateRequest $request, Rating $rating)
    {
        $rating->fill($request->all())->save();
        return redirect('ratings')->with('info', 'Actualización exitosa');
    }

    public function destroy(Rating $rating)
    {
        $rating->delete();
        return back()->with('info', 'Eliminado correctamente');
    }
}
