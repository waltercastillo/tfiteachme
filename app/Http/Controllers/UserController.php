<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Hash;
use Validator;
use Auth;
use Session;

class UserController extends Controller
{

    public function index()
    {
        /*$users = User::orderBy('id', 'DESC')->paginate(15);*/
        $users = User::with('role')->orderBy('id', 'ASC')->paginate(14); //Eager Loads-caraga ansiosa 
        return view('admin.users.index', compact('users'));             //para no generar una consulta por registro
    }

    public function show(User $user)
    {
        return view('admin.users.show', compact('user'));
    }

    public function edit(User $user)
    {
        return view('admin.users.edit', compact('user'));
    }

    public function update(Request $request, User $user)
    {
        /* $user->fill($request->all())->save();*/
        /*$user->name         = $request->name;
        $user->apellido     = $request->apellido;
        $user->sobremi     = $request->sobremi;
        $user->ciudad       = $request->ciudad;
        $user->provincia    = $request->provincia;
        $user->direccion    = $request->direccion;*/
        $user->fill($request->only('name', 'apellido', 'sobremi','ciudad','provincia','direccion'))->save();
        return redirect('users')->with('info', 'usuario actualizado con éxito');      
     } 

    public function destroy(User $user)
    {
        $user->delete();
        return back()->with('info', 'Eliminado correctamente');       
    }

    //para ir a editar la contraseña
    public function password(){
        return View('admin.users.editpassword');
    }

       //para editar la contraseña
    public function updatePassword(Request $request)
    {
        $rules =
        [
            /*'mypassword' => 'required',*/
            'password' => 'required|confirmed|min:3|max:18',
        ];

        $messages =
        [
            'mypassword.required' => 'El campo es requerido',
            'password.required' => 'El campo es requerido',
            'password.confirmed' => 'Los passwords no coinciden',
            'password.min' => 'El mínimo permitido son 6 caracteres',
            'password.max' => 'El máximo permitido son 18 caracteres',
        ];
        
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails())
        {
            return redirect('password')->withErrors($validator);
        }
        else
        {
            if (Hash::check($request->mypassword, Auth::user()->password))
            {
                $user = new User;
                $user->where('email', '=', Auth::user()->email)
                     ->update(['password' => bcrypt($request->password)]);
                Auth::logout();
               /* Session::flush();*/
                 return redirect('login')->with('info', 'Password actualizado con éxito, vuelva a iniciar sesión');
            }
            else
            {
                return redirect('password')->with('message', 'Credenciales incorrectas');
            }
        }
    }

}
