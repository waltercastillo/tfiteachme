<?php

namespace App\Http\Controllers;
use App\Service;
use App\User;
use App\Publication;
use Illuminate\Http\Request;
use App\Http\Requests\service\ServiceUpdateRequest;

class ServiceController extends Controller
{
    public function index()
    {
        $services = Service::with('user')->orderBy('id', 'DESC')->paginate(10);
       /* $services = Service::orderBy('id', 'DESC')->paginate(10);*/
        return view('admin.services.index', compact('services'));
    }

/*    public function create()
    {
        $disciplines = Discipline::orderBy('disciplina', 'ASC')->pluck('disciplina', 'id');
        $levels = Level::orderBy('nivel', 'ASC')->pluck('nivel', 'id');

        return view('admin.services.create', compact('disciplines', 'levels'));  
    }

    public function store(servicestoreRequest $request)
    {
        $Service = Service::create($request->all());
        return redirect('services')->with('info', 'Creada con éxito');      
    }*/

    public function edit(Service $service)
    {
        return view('admin.services.edit', compact('service'));
    }

    public function update(ServiceUpdateRequest $request, Service $service)
    {
        $service->fill($request->all())->save();
        return redirect('services')->with('info', 'Actualización con éxito');
    }

    public function destroy(Service $service)
    {
        $service->delete();
        return back()->with('info', 'Eliminado correctamente');
    }
}
