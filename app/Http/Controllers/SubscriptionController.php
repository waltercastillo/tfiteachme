<?php

namespace App\Http\Controllers;
use App\Http\Requests\subscription\SubscriptionStoreRequest;
use App\Http\Requests\subscription\SubscriptionUpdateRequest;
use App\Subscription;
use Illuminate\Http\Request;

class SubscriptionController extends Controller
{
    public function index()
    {
        $subscriptions = Subscription::orderBy('estado', 'asc')->paginate(3);
        return view('admin.subscriptions.index', compact('subscriptions'));
    }
    
    public function create()
    {
        return view('admin.subscriptions.create');
    }

    public function store(SubscriptionStoreRequest $request)
    {
        $level = Subscription::create($request->all());
        return redirect('subscriptions')->with('info', 'Creada con éxito');      
    }

    public function show(Subscription $subscription)
    {
        return view('admin.users.show', compact('subscription'));
    }

    public function edit(Subscription $subscription)
    {
        return view('admin.subscriptions.edit', compact('subscription'));
    }

    public function update(SubscriptionUpdateRequest $request, Subscription $subscription)
    {
        $subscription->fill($request->all())->save();
        return redirect('subscriptions')->with('info', 'Actualización exitosa');
    }

    public function destroy(Subscription $subscription)
    {
        $subscription->delete();
        return back()->with('info', 'Eliminado correctamente');
    }
}
