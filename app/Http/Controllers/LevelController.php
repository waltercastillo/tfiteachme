<?php

namespace App\Http\Controllers;
use App\Commentary;
use App\Http\Requests\level\LevelStoreRequest;
use App\Http\Requests\level\LevelUpdateRequest;
use App\Level;
use App\User;
use Illuminate\Http\Request;

class LevelController extends Controller
{
    public function index()
    {

         $levels = Level::orderBy('id', 'DESC')->paginate(3);
        return view('admin.levels.index', compact('levels'));

    }

    public function create()
    {
        return view('admin.levels.create');
    }

    public function store(LevelStoreRequest $request)
    {
        $level = Level::create($request->all());
        return redirect('levels')->with('info', 'Creada con éxito');      
    }

    public function edit(Level $level)
    {
        return view('admin.levels.edit', compact('level'));
    }

    public function update(LevelUpdateRequest $request, Level $level)
    {
        $level->fill($request->all())->save();
        return redirect('levels')->with('info', 'Actualización con éxito');
    }

    public function destroy(Level $level)
    {
        $level->delete();
        return back()->with('info', 'Eliminado correctamente');
    }
}
