<?php

namespace App\Http\Controllers;
use App\Discipline;
use App\Level;
use App\Publication;
use Illuminate\Http\Request;
use App\Http\Requests\publication\PublicationUpdateRequest;
use App\Http\Requests\publication\PublicationStoreRequest;

class PublicationController extends Controller
{
    public function index()
    {    
        $publications = Publication::with(['discipline', 'user', 'level'])->paginate(10); //para no tener muchasconsultas juntas n +1
        /*$publications = Publication::orderBy('id', 'DESC')->paginate(10);*/
        return view('admin.publications.index', compact('publications'));
    }

    public function show(Publication $publication)
    {
        return view('admin.publications.show', compact('publication'));
    }

    public function create()
    {
        $disciplines = Discipline::orderBy('disciplina', 'ASC')->pluck('disciplina', 'id');
        $levels = Level::orderBy('nivel', 'ASC')->pluck('nivel', 'id');

        return view('admin.publications.create', compact('disciplines', 'levels'));  
    }

    public function store(PublicationStoreRequest $request)
    {
        $publication = Publication::create($request->all());
        return redirect('publications')->with('info', 'Creada con éxito');      
    }

    public function edit(Publication $publication)
    {

        $disciplines = Discipline::orderBy('disciplina', 'ASC')->pluck('disciplina', 'id');
        $levels = Level::orderBy('nivel', 'ASC')->pluck('nivel', 'id');
        return view('admin.publications.edit', compact('publication', 'disciplines', 'levels'));
    }

    public function update(PublicationUpdateRequest $request, Publication $publication)
    {
        $publication->fill($request->all())->save();
        return redirect('publications')->with('info', 'Actualización con éxito');
    }

    public function destroy(Publication $publication)
    {
        $publication->delete();
        return back()->with('info', 'Eliminado correctamente');
    }
}
