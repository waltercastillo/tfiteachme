<?php

namespace App\Http\Requests\rating;

use App\Rating;
use Illuminate\Foundation\Http\FormRequest;

class RatingUpdateRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
             'puntos'=>'required|max:25|unique:levels,nivel,' . $this->rating->id,
        ];
    }
}
