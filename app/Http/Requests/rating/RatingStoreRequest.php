<?php

namespace App\Http\Requests\rating;

use Illuminate\Foundation\Http\FormRequest;

class RatingStoreRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
             'nivel'=>'required|unique:levels,nivel|max:25',
        ];
    }
}
