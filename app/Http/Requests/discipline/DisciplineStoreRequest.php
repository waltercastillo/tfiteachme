<?php

namespace App\Http\Requests\discipline;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
class DisciplineStoreRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
             'disciplina'=>'required|unique:disciplines|max:25',
        ];
    }
}
