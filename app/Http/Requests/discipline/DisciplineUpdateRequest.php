<?php
namespace App\Http\Requests\discipline;
use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;

class DisciplineUpdateRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }
    
    public function rules()
    {
            return [
             'disciplina'=>'required|max:25|unique:disciplines,disciplina,'.$this->discipline->id,
            ];
    }
}
