<?php

namespace App\Http\Requests\service;
use Illuminate\Foundation\Http\FormRequest;

class ServiceUpdateRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
				'estado'=>'required',
				'fecha_inicio'    => 'date_format:"Y-m-d H:i:s"|required',
				'fecha_final'    => 'date_format:"Y-m-d H:i:s"|required',
       ];
    }
}