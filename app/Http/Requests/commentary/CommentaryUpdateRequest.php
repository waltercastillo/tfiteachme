<?php

namespace App\Http\Requests\commentary;
use Illuminate\Foundation\Http\FormRequest;

class CommentaryUpdateRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
				'comentario'=>'required|min:5|max:400',
				'fecha_hora'    => 'date_format:"Y-m-d H:i:s"|required',
       ];
    }
}