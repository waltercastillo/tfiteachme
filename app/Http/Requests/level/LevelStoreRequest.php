<?php

namespace App\Http\Requests\level;

use Illuminate\Foundation\Http\FormRequest;

class LevelStoreRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
             'nivel'=>'required|unique:levels,nivel|max:25',
        ];
    }
}
