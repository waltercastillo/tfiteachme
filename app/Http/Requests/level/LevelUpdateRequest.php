<?php

namespace App\Http\Requests\level;

use App\Level;
use Illuminate\Foundation\Http\FormRequest;

class LevelUpdateRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
             'nivel'=>'required|max:25|unique:levels,nivel,' . $this->level->id,
        ];
    }
}
