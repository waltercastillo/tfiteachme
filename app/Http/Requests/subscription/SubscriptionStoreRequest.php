<?php

namespace App\Http\Requests\subscription;

use Illuminate\Foundation\Http\FormRequest;

class SubscriptionStoreRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'nombre'=>'required|unique:subscriptions,nombre|Min:1',
            'precio'=>'required|numeric|Integer|Min:1',
            'tiempo'=>'numeric|required|Integer|Min:1',
            'cant_publicacion'=>'numeric|required|Integer|Min:1',
            'estado'=>'required',
        ];
    }
}
