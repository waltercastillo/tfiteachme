<?php

namespace App\Http\Requests\subscription;

use App\Rating;
use Illuminate\Foundation\Http\FormRequest;

class SubscriptionUpdateRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
             /*'puntos'=>'required|max:25|unique:levels,nivel,' . $this->rating->id,*/
            'nombre'=>'required|Min:1|unique:subscriptions,nombre,'. $this->subscription->id,
            'precio'=>'required|numeric|Integer|Min:1',
            'tiempo'=>'numeric|required|Integer|Min:1',
            'cant_publicacion'=>'numeric|required|Integer|Min:1',
            'estado'=>'required',
        ];
    }
}
