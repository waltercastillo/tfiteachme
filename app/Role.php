<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
	
	
	const ADMINISTRADOR = 1;	//definir constante antes 
	const ESTUDIANTE 	= 2;	//de ejecutar 
	const PROFESOR 		= 3;	//las migraciones

	/* protected $fillable = ['rol'];*/

    public function users()  
    {
        return $this->hasMany('App\User');
    }
}
