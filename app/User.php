<?php

namespace App;

use Iatstuti\Database\Support\CascadeSoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use Notifiable;

    use SoftDeletes, CascadeSoftDeletes;  

    protected $cascadeDeletes = ['ratings', 'publications', 'commentaries', 'services'];

    protected $dates = ['deleted_at']; 

    protected $fillable = ['name','apellido', 'email', 'password', 'sobremi', 'avatar', 'ciudad', 'provincia', 'direccion' ];
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function role()
    {
        return $this->belongsTo('App\Role');
    }

    public function disciplines()
    {
        return $this->belongsToMany('App\Discipline');
    }

    public function publications()
    {
        return $this->hasMany('App\Publication');
    }

    public function services()
    {
        return $this->hasMany('App\Service');
    }

    public function commentaries()
    {
        return $this->hasMany('App\Commentary');
    }

    public function ratings()
    {
        return $this->hasMany('App\Rating');
    }


}
