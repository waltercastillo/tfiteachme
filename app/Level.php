<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Level extends Model
{
	    use SoftDeletes; 

   
	protected $dates = ['deleted_at']; 
	protected $fillable = ['nivel'];
    public function publications()
    {
        return $this->hasMany('App\Publication');
    }
}
