<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Commentary extends Model
{

    use SoftDeletes; 
    
    protected $fillable = ['fecha_hora', 'comentario', 'user_id', 'publication_id'];

    protected $dates = ['deleted_at', 'fecha_hora']; 
    
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function publication()
    {
        return $this->belongsTo('App\Publication');
    }

}
