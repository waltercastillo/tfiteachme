<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Rating extends Model
{
    use SoftDeletes; 

    protected $dates = ['deleted_at']; 
    protected $fillable = ['puntos', 'comentario', 'user_id'];
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
