<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Publication extends Model
{
        use SoftDeletes; 

    protected $dates = ['deleted_at']; 
protected $fillable = ['descripcion', 'horario', 'ciudad', 'provincia', 'direccion', 'lat', 'lng', 'tipo', 'estado', 'discipline_id', 'user_id', 'level_id', 'created_at', 'updated_at'];

    const DESACTIVADA   = 'desactivada';
    const ACTIVADA      = 'activada';
    const FREE          = 'free';
    const PREMIUM       = 'premium';



    public function discipline()
    {
        return $this->belongsTo('App\Discipline');
    }

    public function level()
    {
        return $this->belongsTo('App\Level');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function commentaries()
    {
        return $this->hasMany('App\Commentary');
    }

    public function services()
    {
        return $this->hasMany('App\Service');
    }

    public function subscriptions()
    {
        return $this->belongsToMany('App\Subscription')->withPivot('nombre', 'precio', 'tiempo', 'cant_publicacion')->withTimestamps();
    }

}
