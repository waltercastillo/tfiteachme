<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Discipline extends Model
{
	 use SoftDeletes; 
	protected $dates = ['deleted_at'];
	protected $fillable = ['disciplina'];

    public function publicatons()
    {
        return $this->hasMany('App\Publication');
    }
}
