<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Service extends Model
{
    use SoftDeletes; 

    protected $fillable = ['estado', 'fecha_inicio', 'fecha_final', 'user_id', 'publication_id'];

    protected $dates = ['deleted_at', 'fecha_inicio', 'fecha_final']; 

	const ENCURSO   	= 'encurso';
    const FINALIZADA    = 'finalizada';
    
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function publication()
    {
        return $this->belongsTo('App\publication');
    }

}