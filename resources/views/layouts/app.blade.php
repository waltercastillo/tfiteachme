<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="{{{ asset('img/favicon.png') }}}">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    
    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css">

    <!-- Styles -->
    <style>


    </style>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <div class="navtop">
            <nav class="navbar navbar-expand-md navbar-light navbar-laravel fixed-top">
                <div class="container">
                    <a class="navbar-brand" href="{{ url('/') }}">
                        {{ config('app.name', 'Laravel') }}
                    </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <!-- Lado izquierdo de Navbar -->
                   @auth
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item {{ active('users*')}}"><a class="nav-link" href="{{ route('users.index') }}">Usuarios</a></li>

                            <li class="nav-item {{ active('disciplines*')}}"><a class="nav-link" href="{{ route('disciplines.index') }}">Disciplina</a></li>

                            <li class="nav-item {{ active('levels*')}}"><a class="nav-link" href="{{ route('levels.index') }}">Niveles</a></li>

                            <li class="nav-item {{ active('ratings*')}}"><a class="nav-link" href="{{ route('ratings.index') }}">Ratings</a></li>

                            <li class="nav-item {{ active('subscriptions*')}}"><a class="nav-link" href="{{ route('subscriptions.index') }}">Planes</a></li>

                            <li class="nav-item {{ active('publications*')}}"><a class="nav-link" href="{{ route('publications.index') }}">Publicaciones</a></li> 

                            <li class="nav-item {{ active('services*')}}"><a class="nav-link" href="{{ route('services.index') }}">Servicios</a></li> 

                            <li class="nav-item {{ active('commentaries*')}}"><a class="nav-link" href="{{ route('commentaries.index') }}">Comentarios</a></li>     
                        </ul>
                   @endauth 
                        

                        <!-- Lado derecho de Navbar -->
                        <ul class="navbar-nav ml-auto">

                         <!-- Authentication Links -->
                            @guest
                                <li><a class="nav-link" href="{{ route('login') }}">{{ ('Ingresar') }}</a></li>
                                <li><a class="nav-link" href="{{ route('register') }}">{{ ('Registrarse') }}</a></li>
                            @else
                                <li class="nav-item"><a class="nav-link" href="{{ route('publications.create') }}">Crear public</a></li> 

                                <li class="nav-item dropdown">
                                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                        {{ Auth::user()->name }} <span class="caret"></span>
                                    </a>

                                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                                           Salir
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>

                                        <a class="dropdown-item" href="{{ route('password') }}">
                                            Cambiar password
                                        </a>

                                        <a class="dropdown-item" href="{{ route('users.edit',  auth()->user())}}">
                                            Editar perfil
                                        </a>
                                        <a class="dropdown-item" href="#">
                                            Cambiar avatar
                                        </a>
                                    </div>
                                </li>
                            @endguest
                                   <div class="flex-center position-ref full-height">


                        {{--    @if (Route::has('login'))
                                <div class="top-right links">
                                    @auth
                                        <a href="{{ url('/home') }}">Home</a>
                                    @else
                                        <a href="{{ route('login') }}">Login</a>
                                        <a href="{{ route('register') }}">Register</a>
                                    @endauth
                                </div>
                            @endif--}}
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
        


<main class="py-4 my-5">
    
    {{-- para ocultar mostrar  mensaje de aviso --}}
    <div class="container center-block col-12 col-md-8" id="ocultar">
        @if (session('info'))
            <div class="alert alert-success">
                <h5>{{ session('info') }}</h5>
            </div>
        @endif
    </div>{{-- fin  --}}

    @yield('content')
</main>
</div>
<footer class="footer">
    <h5 class="py-2">TeachMe-2018</h5>
</footer>


<script type="text/javascript">
    $(document).ready(function() {
        setTimeout(function() {
            $("#ocultar").fadeOut(800);
        },3500);
    });
</script> 
</body>
</html>

{{--        <script>
        $(document).ready(function() {
          alert("jQuery esta funcionando !!");
        });
    </script> --}}