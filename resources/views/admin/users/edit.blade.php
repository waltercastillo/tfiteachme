@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-12 col-md-8">
            <div class="card">
                <div class="card-header">
                	<strong>Editar: {{$user->email}}</strong>
					{{-- <a class="btn btn-sm btn-success float-right" ><i class="fas fa-plus"></i></a> --}}
                </div>
                <div class="card-body">
					{!! Form::model($user, ['route' => ['users.show', $user], 
					'method' => 'PUT', 'files' => true]) !!}

                        <div class="form-group">
                        <strong>{{ Form::label('name', 'Nombre: ') }}</strong>
                        {{ Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Nombre']) }}
                        </div>

                        <div class="form-group">
                        {{ Form::label('apellido', 'Apellido: ') }}
                        {{ Form::text('apellido', null, ['class' => 'form-control', 'placeholder' => 'Nombre']) }}
                        </div>
                        {{-- <div class="form-group">
                        {{ Form::label('email', 'Email: ') }}
                        {{ Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'Nombre']) }}
                        </div> --}}
{{--                         <div class="form-group">
                        {{ Form::label('password', 'Contraseña: ') }}
                        {{ Form::password('password', ['class' => 'form-control', 'placeholder' => 'Nombre']) }}
                        </div> --}}
                        <div class="form-group">
                        {{ Form::label('sobremi', 'Sobre mi: ') }}
                        {{ Form::textarea('sobremi', null, ['class' => 'form-control', 'placeholder' => 'Nombre', 'rows' => 2]) }}
{{--                         </div>
                        <div class="form-group">
                        {{ Form::label('avatar', 'Avatar: ') }}
                        {{ Form::file('avatar',['class' => 'form-control']) }}
                        </div> --}}
                        <div class="form-group">
                        {{ Form::label('ciudad', 'Ciudad: ') }}
                        {{ Form::text('ciudad', null, ['class' => 'form-control', 'placeholder' => 'Nombre']) }}
                        </div>
                        <div class="form-group">
                        {{ Form::label('provincia', 'Provincia: ') }}
                        {{ Form::text('provincia', null, ['class' => 'form-control', 'placeholder' => 'Nombre']) }}
                        </div>
                        <div class="form-group">
                        {{ Form::label('direccion', 'Dirección: ') }}
                        {{ Form::text('direccion', null, ['class' => 'form-control', 'placeholder' => 'Nombre']) }}
                        </div>
                        <div class="form-group text-center">
                        {{ Form::submit('Guardar', ['class' => 'btn btn-primary btn-lg btn-block']) }}

                        
					{!! Form::close() !!}
                            <a href="{{ route('users.index') }}" class="btn btn-outline-info mt-2"><i class="fas fa-long-arrow-alt-left"></i></a>
                        </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection