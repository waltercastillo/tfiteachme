@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-12 col-md-8">
            
                            <div class="card-header  text-center">
                    <strong>{{$user->id}} - {{$user->apellido}} , {{$user->name}}.</strong>

                </div>
                <div class="card-body text-center">
                    <hr>
                    <p><strong>Rol: </strong>{{$user->role->rol}}</p>
                    <hr>
                    <p><strong>Email: </strong>{{$user->email}}</p>
                    <hr>
                    <p><strong>Provicia: </strong>{{$user->provincia}}</p>
                    <hr>
                    <p><strong>Ciudad: </strong>{{$user->ciudad}}</p>
                    <hr>
                    <p><strong>Dirección: </strong>{{$user->direccion}}</p>
                    <hr>
                    <p><strong>Sobre Mi: </strong>{{$user->sobremi}}</p>
                    <hr>
                    <a href="{{ URL::previous() }}" class="btn btn-outline-info mt-2"><i class="fas fa-long-arrow-alt-left"></i></a>
                </div>
        </div>
    </div>
</div>

@endsection
{{-- ***********************
<div class="col-md-8 col-md-offset-2">
        	<h1>{{ $post->name }}</h1>
        	
            <div class="panel panel-default">
                <div class="panel-heading">
                    Categoria: 
                    <a href="{{ route('category', $post->category->slug) }}">{{ $post->category->name }}</a>
                </div>
                <div class="panel-body">
                    @if($post->file)
                        <img src="{{ $post->file }}" class="img-responsive">
                    @endif
                    {{ $post->excerpt }}
                    <hr>
                    {!! $post->body !!}                    
                    <hr>
                    Etiquetas: 
                    @foreach($post->tags as $tag)
                        <a href="{{ route('tag', $tag->slug) }}">
                            {{ $tag->name }}
                        </a>
                    @endforeach
                </div>
            </div>
        </div>
**********************

	{{ Form::label('sobremi', 'Sobre mi') }}
	{{ Form::textarea('sobremi', null, ['class' => 'form-control', 'placeholder' => 'Nombre']) }}


	{{ Form::label('ciudad', 'Ciudad') }}
	{{ Form::text('ciudad', null, ['class' => 'form-control', 'placeholder' => 'Nombre']) }}

	{{ Form::label('provincia', 'Provincia') }}
	{{ Form::text('provincia', null, ['class' => 'form-control', 'placeholder' => 'Nombre']) }}

	{{ Form::label('direccion', 'Dirección') }}
	{{ Form::text('direccion', null, ['class' => 'form-control', 'placeholder' => 'Nombre']) }}
 --}}