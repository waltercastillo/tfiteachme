@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                	<strong>Lista de Usuarios :</strong>
					{{-- <a class="btn btn-sm btn-success float-right" ><i class="fas fa-plus"></i></a> --}}
                </div>
                <div class="card-body table-responsive">
					<table class="table table-hover">
						<thead>
				    		<tr>
								<th width="5px">ID</th>
								<th>Nombre</th>
								<th>Apellido</th>
								<th>Email</th>							
								<th>Rol</th>
								<th colspan="3">&nbsp;</th>
							</tr>
						</thead>
						<tbody>
							@foreach($users as $user)
							<tr>
								<td>{{ $user->id }}</td>
								<td>{{ $user->name }}</td>
								<td>{{ $user->apellido }}</td>
								<td>{{ $user->email }}</td>
								<td>{{ $user->role->rol }}</td>
							 
								<td width="10px">
									<a href="{{ route('users.show', $user) }}" class="btn btn-primary btn-sm">
										<i class="fas fa-eye"></i>
									</a>
								</td>
								<td width="10px">
									<a href="{{ route('users.edit', $user) }}" type="button" class="btn btn-warning btn-sm">
										<i class="fas fa-edit"></i>
									</a>
								</td>
								<td width="10px">
									{!! Form::open(['route' => ['users.destroy', $user->id],
									'method'=> 'DELETE']) !!}
										<button class="btn btn-sm btn-danger" onclick="return confirm('estas seguro')">
											<i class="fas fa-trash-alt"></i>
										</button>
									{!! Form::close() !!}
								</td>
								{{-- <td width="10px">
									{!! Form::open(['route' => ['users.destroy', $user],
									'method'=> 'DELETE']) !!}
										<a href="{{ route('users.destroy', $user) }} type="button" class="btn btn-danger btn-sm">
											<i class="fas fa-trash-alt"></i>
										</a>
									{!! Form::close() !!}
								</td> --}}
							</tr>
							@endforeach
						</tbody>
					</table>
					{{ $users->render() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection