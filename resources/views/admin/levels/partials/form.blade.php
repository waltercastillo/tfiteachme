<div class="form-group">
	<strong>{{ Form::label('nivel', 'Nivel: ') }}</strong>
	{{ Form::text('nivel',  null, ['class' => 'form-control', 'placeholder' => 'Nivel...']) }}
</div>

<div class="form-group">
	{{ Form::submit('Guardar', ['class' => 'btn btn-primary btn-lg btn-block']) }}
</div>

<a href="{{ route('levels.index') }}" class="btn btn-outline-info mt-2"><i class="fas fa-long-arrow-alt-left"></i></a>
