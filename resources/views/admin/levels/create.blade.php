@extends('layouts.app')
@section('content')
<div class="container">
	<div class="row justify-content-center">
		<div class="col-12 col-md-8">
			<div class="card">
                <div class="card-header">
					<strong>Crear Nivel :</strong>
				</div>
				<div class="card-body  text-center">
					@include('error')
					{!! Form::open(['route' => 'levels.store']) !!}
						@include('admin.levels.partials.form')
					{!! Form::close() !!}
                </div>
			</div>
		</div>
	</div>
</div>
@endsection


