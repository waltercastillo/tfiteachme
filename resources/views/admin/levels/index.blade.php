@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-12 col-md-8">
            <div class="card">
                <div class="card-header">
                	<strong>Lista de niveles :</strong>
					<a href="{{ route('levels.create') }}" type="button" class="btn btn-sm btn-success float-right" ><i class="fas fa-plus"></i></a>
                </div>
                <div class="card-body table-responsive">
					<table class="table table-hover">
						<thead>
				    		<tr>
								<th width="5px">ID</th>
								<th>Disciplina</th>
								<th colspan="3">&nbsp;</th>
							</tr>
						</thead>
						<tbody>
							@foreach($levels as $level)
							<tr>
								<td>{{ $level->id }}</td>
								<td>{{ $level->nivel }}</td>
							 
								<td width="10px">
									<a href="{{ route('levels.edit', $level) }}" type="button" class="btn btn-warning btn-sm">
										<i class="fas fa-edit"></i>
									</a>
								</td>
								<td width="10px">
									{!! Form::open(['route' => ['levels.destroy', $level->id],
									'method'=> 'DELETE']) !!}
										<button class="btn btn-sm btn-danger">
											<i class="fas fa-trash-alt"></i>
										</button>
									{!! Form::close() !!}
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
					{{ $levels->render() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection