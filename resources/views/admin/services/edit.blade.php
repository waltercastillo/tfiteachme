@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-12 col-md-6">
            <div class="card">
                <div class="card-header">

                    <strong>Servicio ID N°:{{$service->id}} </strong>
                </div>
                <div class="card-body  text-center">
                    @include('error')
                    {!! Form::model($service, ['route' => ['services.update', $service], 
                    'method' => 'PUT']) !!}
                    
                        @include('admin.services.partials.form')

                        
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
