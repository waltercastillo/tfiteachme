

<div class="form-group">
	<strong>{{ Form::label('estado', 'Estado:') }}</strong><br>
	<label>
		{{ Form::radio('estado', 'encurso') }} En curso
	</label>
	<label>
		{{ Form::radio('estado', 'finalizada') }} Finalizado
	</label>
</div>
<hr>

<div class="form-group">
<strong>{{ Form::label('fecha_inicio', 'Fecha de inicio: ') }}</strong>
    {{ Form::text('fecha_inicio',  null, ['class' => 'form-control', 'required']) }}
</div>

<div class="form-group">
    <strong>{{ Form::label('fecha_final', 'Fecha de final: ') }}</strong>
    {{ Form::text('fecha_final', null, ['class' => 'form-control', 'required']) }}
</div>


<div class="form-group">
	{{ Form::submit('Guardar', ['class' => 'btn btn-primary btn-lg btn-block']) }}
</div>
<a href="{{ route('services.index') }}" class="btn btn-outline-info mt-2"><i class="fas fa-long-arrow-alt-left"></i></a>


