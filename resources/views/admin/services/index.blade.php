@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-12 col-md-8">
            <div class="card">
                <div class="card-header">
                	<strong>Lista de Servicios:</strong>
                </div>
                <div class="card-body table-responsive">
					<table class="table table-hover">
						<thead>
				    		<tr>
								<th width="5px">ID</th>
								<th>Estado</th>
								<th>Inicio</th>
								<th>Final</th>
								<th>Id alum</th>
								<th>Id publ.</th>
								<th colspan="3">&nbsp;</th>
							</tr>
						</thead>
						<tbody>
							@foreach($services as $service)
							<tr>
								<td>{{ $service->id }}</td>
								<td>{{ $service->estado }}</td>
								<td>{{ $service->fecha_inicio->format('d/m/Y') }}</td>
								<td>{{ $service->fecha_final->format('d/m/Y') }}</td>
								<td>{{ $service->user->name }}</td>
								<td>{{ $service->publication_id }}</td>

								<td width="10px">
									<a href="{{ route('services.edit', $service) }}" type="button" class="btn btn-warning btn-sm">
										<i class="fas fa-edit"></i>
									</a>
								</td>
								<td width="10px">
									{!! Form::open(['route' => ['services.destroy', $service->id],
									'method'=> 'DELETE']) !!}
										<button class="btn btn-sm btn-danger">
											<i class="fas fa-trash-alt"></i>
										</button>
									{!! Form::close() !!}
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
					{{ $services->render() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection