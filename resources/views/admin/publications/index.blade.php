@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-12 col-md-8">
            <div class="card">
                <div class="card-header">
                	<strong>Lista de Publicaciones:</strong>
                </div>
                <div class="card-body table-responsive">
					<table class="table table-hover">
						<thead>
				    		<tr>
								<th width="5px">ID</th>
								<th>Correo</th>
								<th>Disciplina</th>
								<th>Nivel</th>
								<th width="8px">Estado</th>
								<th colspan="3">&nbsp;</th>
							</tr>
						</thead>
						<tbody>

							@foreach($publications as $publication)
							<tr>
								{{-- {{dd($publication)}} --}}
								<td>{{ $publication->id }}</td>
								<td>{{ $publication->user->email }}</td>
								<td>{{ $publication->discipline->disciplina }}</td>
								<td>{{ $publication->level->nivel }}</td>
								<td>{{ $publication->estado }}</td>

								<td width="10px">
									<a href="{{ route('publications.show', $publication) }}" class="btn btn-primary btn-sm">
										<i class="fas fa-eye"></i>
									</a>
								</td>

								<td width="10px">
									<a href="{{ route('publications.edit', $publication) }}" type="button" class="btn btn-warning btn-sm">
										<i class="fas fa-edit"></i>
									</a>
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
					{{ $publications->render() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection