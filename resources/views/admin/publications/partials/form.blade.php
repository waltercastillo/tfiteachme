<div class="form-group">
	<strong>{{ Form::label('descripcion', 'Descripcion: ') }}</strong>
	{{ Form::text('descripcion',  null, ['class' => 'form-control', 'placeholder' => 'Descripcion...', 'required']) }}
</div>
<div class="form-group">
	<strong>{{ Form::label('discipline_id', 'Disciplina:') }}</strong>
	{{ Form::select('discipline_id', $disciplines, null, ['class' => 'form-control', 'placeholder' => 'Seleccione una disciplina...', 'required']) }}
</div> 

<div class="form-group">
	<strong>{{ Form::label('level_id', 'Niveles:') }}</strong>
	{{ Form::select('level_id', $levels, null, ['class' => 'form-control', 'placeholder' => 'Seleccione un nivel...']) }}
</div> 

<div class="form-group">
	<strong>{{ Form::label('horario', 'Horario: ') }}</strong>
	{{ Form::text('horario',  null, ['class' => 'form-control', 'placeholder' => 'ej: 08 a 12...', 'required']) }}
</div>

<div class="form-group">
	<strong>{{ Form::label('ciudad', 'Ciudad: ') }}</strong>
	{{ Form::text('ciudad',  null, ['class' => 'form-control', 'placeholder' => 'ej: Rosario...', 'required']) }}
</div>

<div class="form-group">
	<strong>{{ Form::label('provincia', 'Provincia: ') }}</strong>
	{{ Form::text('provincia',  null, ['class' => 'form-control', 'placeholder' => 'ej: Santa Fe...', 'required']) }}
</div>

<div class="form-group">
	<strong>{{ Form::label('direccion', 'Dirección: ') }}</strong>
	{{ Form::text('direccion',  null, ['class' => 'form-control', 'placeholder' => 'ej: san martin 555...', 'required']) }}
</div>

<div class="form-group">
	<strong>{{ Form::label('lat', 'Latitud: ') }}</strong>
	{{ Form::text('lat',  null, ['class' => 'form-control', 'placeholder' => 'ej: 30.3323...', 'required']) }}
</div>

<div class="form-group">
	<strong>{{ Form::label('lng', 'Longitud: ') }}</strong>
	{{ Form::text('lng',  null, ['class' => 'form-control', 'placeholder' => 'ej: 30.3323...', 'required']) }}
</div>
{{ Form::button('Seleccionar cordenadas', ['class' => 'btn btn-outline-secondary']) }}
<hr>
<div class="form-group">
	<strong>{{ Form::label('estado', 'Estado:') }}</strong><br>
	<label>
		{{ Form::radio('estado', 'activada') }} Activar
	</label>
	<label>
		{{ Form::radio('estado', 'desactivada') }} Desactivar
	</label>
</div>
<hr>
<div class="form-group">
	<strong>{{ Form::label('tipo', 'Tipo:') }}</strong><br>
	<label>
		{{ Form::radio('tipo', 'free') }} Free
	</label>
	<label>
		{{ Form::radio('tipo', 'premium') }} Premium
	</label>
</div>




