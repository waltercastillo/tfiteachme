@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-12 col-md-8">
            <div class="card">
                <div class="card-header">
                    <strong>Crear publicación</strong>
                </div>
                <div class="card-body  text-center">

                    @include('error')
                    {!! Form::open(['route' => 'publications.store']) !!}
                        
                        {{ Form::hidden('user_id', auth()->user()->id) }}
                
                         @include('admin.publications.partials.form')

                        <div class="form-group">
                        {{ Form::submit('Guardar', ['class' => 'btn btn-primary btn-lg btn-block']) }}
                        </div>

                        <a href="{{ route('publications.index') }}" class="btn btn-outline-info mt-2"><i class="fas fa-long-arrow-alt-left"></i></a>
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
