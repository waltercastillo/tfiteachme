@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-12 col-md-8">
            <div class="card">
                <div class="card-header  text-center">
                	<strong>Publicación ID N°:{{$publication->id}} </strong>
                </div>
                <div class="card-body text-center">
					<p><strong>Profesor: </strong>{{$publication->user_id}} - {{$publication->user->name}}, {{$publication->user->apellido}}</p>
					<hr><p><strong>Descripción:</strong>{{$publication->descripcion}}</p>
					<hr><p><strong>Horario:</strong>{{$publication->horario}}</p>
                    <hr><p><strong>Ciudad:</strong>{{$publication->ciudad}}</p>
                    <hr><p><strong>Provincia:</strong>{{$publication->provincia}}</p>
                    <hr><p><strong>Direccion:</strong>{{$publication->direccion}}</p>
                    <hr><p><strong>latitud:</strong>{{$publication->lat}}</p>
                    <hr><p><strong>Longitud:</strong>{{$publication->lng}}</p>
                    <hr><p><strong>Tipo:</strong>{{$publication->tipo}}</p>
                    <hr><p><strong>Estado:</strong>{{$publication->estado}}</p>
                    <hr><p><strong>Disciplina:</strong>{{$publication->discipline->disciplina}}</p>
                    <hr><p><strong>Nivel:</strong>{{$publication->level->nivel}}</p>
                    <hr><p><strong>Fecha de creación: </strong>{{$publication->created_at->format('d/m/Y H:i:s')}}</p>
					<hr><p><strong>Ultima actualización: </strong>{{$publication->updated_at->format('d/m/Y h:i:s')}}</p>
					<hr>
                    <a href="{{ URL::previous() }}" class="btn btn-outline-info mt-2"><i class="fas fa-long-arrow-alt-left"></i></a>
                </div>
            </div>
            </div>
        </div>
    </div>
</div>

@endsection
