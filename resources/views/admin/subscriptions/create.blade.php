@extends('layouts.app')
@section('content')
<div class="container">
	<div class="row justify-content-center">
		<div class="col-12 col-md-8">
			<div class="card">
                <div class="card-header">
					<strong>Crear plan:</strong>
				</div>
				<div class="card-body  text-center">
					@include('error')
					{!! Form::open(['route' => 'subscriptions.store']) !!}
						@include('admin.subscriptions.partials.form')
					{!! Form::close() !!}
                </div>
			</div>
		</div>
	</div>
</div>
@endsection


