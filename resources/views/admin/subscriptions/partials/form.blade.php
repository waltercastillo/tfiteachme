<div class="form-group">
	<strong>{{ Form::label('nombre', 'Plan: ') }}</strong>
	{{ Form::text('nombre',  null, ['class' => 'form-control', 'placeholder' => 'Nombre del plan...']) }}
</div>

<div class="form-group">
	<strong>{{ Form::label('precio', 'Precio:') }}</strong>
	{{ Form::text('precio',  null, ['class' => 'form-control', 'placeholder' => 'Precio del plan...']) }}
</div>

<div class="form-group">
	<strong>{{ Form::label('tiempo', 'Cant Días: ') }}</strong>
	{{ Form::text('tiempo',  null, ['class' => 'form-control', 'placeholder' => 'Días...']) }}
</div>

<div class="form-group">
	<strong>{{ Form::label('cant_publicacion', 'cant public.: ') }}</strong>
	{{ Form::text('cant_publicacion',  null, ['class' => 'form-control', 'placeholder' => 'cant de publica...']) }}
</div>

<div class="form-group">
	{{ Form::label('estado', 'Estado:') }}
	<label>
		{{ Form::radio('estado', 'activado') }} Plan activo:
	</label>
	<label>
		{{ Form::radio('estado', 'desactivado') }} Plan desactivado:
	</label>
</div>
<div class="form-group">
	{{ Form::submit('Guardar', ['class' => 'btn btn-primary btn-lg btn-block']) }}
</div>

<a href="{{ route('subscriptions.index') }}" class="btn btn-outline-info mt-2"><i class="fas fa-long-arrow-alt-left"></i></a>