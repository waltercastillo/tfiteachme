@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-12 col-md-8">
            <div class="card">
                <div class="card-header">
                	<strong>Lista de Planes:</strong>
                	<a href="{{ route('subscriptions.create') }}" type="button" class="btn btn-sm btn-success float-right" ><i class="fas fa-plus"></i></a>
                </div>
                <div class="card-body table-responsive">
					<table class="table table-hover">
						<thead>
				    		<tr>
								<th>ID</th>
								<th>Plan</th>
								<th>Precio</th>
								<th>Tiempo</th>
								<th>Cant.</th>					
	{{-- 							<th width="8px">Estado</th> --}}
								<th colspan="3">&nbsp;</th>
							</tr>
						</thead>
						<tbody>
							@foreach($subscriptions as $subscription)
							<tr>
								<td>{{ $subscription->id }}</td>
								<td>{{ $subscription->nombre }}</td>
								<td>{{ $subscription->precio }}</td>
								<td>{{ $subscription->tiempo }}</td>
								<td>{{ $subscription->cant_publicacion }}</td>

								<td width="10px">
									<a href="{{ route('subscriptions.edit', $subscription) }}" type="button" class="btn btn-warning btn-sm">
										<i class="fas fa-edit"></i>
									</a>
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
					{{ $subscriptions->render() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection