@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-12 col-md-8">
            <div class="card">
                <div class="card-header  text-center">
                	<strong>{{$rating->id}} - {{$rating->apellido}} , {{$rating->name}}.</strong>
					{{-- <a class="btn btn-sm btn-success float-right" ><i class="fas fa-plus"></i></a> --}}
                </div>
                <div class="card-body text-center">
					<hr>
					<p><strong>Rol: </strong>{{$rating->role->rol}}</p>
					<hr>
					<p><strong>Email: </strong>{{$rating->email}}</p>
					<hr>
					<p><strong>Provicia: </strong>{{$rating->provincia}}</p>
					<hr>
					<p><strong>Ciudad: </strong>{{$rating->ciudad}}</p>
					<hr>
					<p><strong>Dirección: </strong>{{$rating->direccion}}</p>
					<hr>
					<p><strong>Sobre Mi: </strong>{{$rating->sobremi}}</p>
                    <hr>
                    <a href="{{ URL::previous() }}" class="btn btn-outline-info mt-2"><i class="fas fa-long-arrow-alt-left"></i></a>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
