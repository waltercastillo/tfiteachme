@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-12 col-md-8">
            <div class="card">
                <div class="card-header  text-center">
                	<strong>Comentario ID N°:{{$commentary->id}} </strong>
                </div>
                <div class="card-body text-center">
					<p><strong>Id usuario/email: </strong>{{$commentary->user_id}} - {{$commentary->user->email}}</p>
					<hr><p><strong>Comentario:</strong>{{$commentary->comentario}}</p>
                    <hr><p><strong>Id Publ:</strong>{{$commentary->publication_id}}</p>
               
					<hr><p><strong>Fecha: </strong>{{$commentary->fecha_hora->format('d/m/Y h:i:s')}}</p>
					<hr>
                    <a href="{{ URL::previous() }}" class="btn btn-outline-info mt-2"><i class="fas fa-long-arrow-alt-left"></i></a>
                </div>
            </div>
            </div>
        </div>
    </div>
</div>

@endsection
