
<div class="form-group">
	<strong>{{ Form::label('comentario', 'Sobre mi: ') }}</strong>
	{{ Form::textarea('comentario', null, ['class' => 'form-control', 'placeholder' => 'Ej: hola...', 'rows' => 2]) }}
</div>

<div class="form-group">
    <strong>{{ Form::label('fecha_hora', 'Fecha: ') }}</strong>
    {{ Form::text('fecha_hora', null, ['class' => 'form-control', 'required']) }}
</div>


<div class="form-group">
	{{ Form::submit('Guardar', ['class' => 'btn btn-primary btn-lg btn-block']) }}
</div>
<a href="{{ route('commentaries.index') }}" class="btn btn-outline-info mt-2"><i class="fas fa-long-arrow-alt-left"></i></a>

