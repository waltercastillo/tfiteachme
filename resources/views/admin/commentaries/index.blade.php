@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-12 col-md-12">
            <div class="card">
                <div class="card-header">
                	<strong>Lista de Commentaries:</strong>
                </div>
                <div class="card-body table-responsive">
					<table class="table table-hover">
						<thead>
				    		<tr>
								<th width="5px">ID</th>
								<th>Fecha</th>
								<th>Comentario</th>
								<th>id-Usuario</th>
								<th>Id publ.</th>
								<th colspan="3">&nbsp;</th>
							</tr>
						</thead>
						<tbody>
							@foreach($commentaries as $commentary)
							<tr>
								<td>{{ $commentary->id }}</td>
								<td>{{ $commentary->fecha_hora->format('d/m/Y') }}</td>
								<td>{{ str_limit($commentary->comentario, 8)}}</td>
								<td>{{ $commentary->user_id }} - {{ $commentary->user->email}}</td>
								<td>{{ $commentary->publication_id }}</td>


								<td width="10px">
									<a href="{{ route('commentaries.show', $commentary) }}" class="btn btn-primary btn-sm">
										<i class="fas fa-eye"></i>
									</a>
								</td>

								<td width="10px">
									<a href="{{ route('commentaries.edit', $commentary) }}" type="button" class="btn btn-warning btn-sm">
										<i class="fas fa-edit"></i>
									</a>
								</td>
								<td width="10px">
									{!! Form::open(['route' => ['commentaries.destroy', $commentary->id],
									'method'=> 'DELETE']) !!}
										<button class="btn btn-sm btn-danger">
											<i class="fas fa-trash-alt"></i>
										</button>
									{!! Form::close() !!}
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
					{{ $commentaries->render() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection