@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-12 col-md-8">
            <div class="card">
                <div class="card-header">
                	<strong>Lista de Disciplinas :</strong>
					<a href="{{ route('disciplines.create') }}" type="button" class="btn btn-sm btn-success float-right" ><i class="fas fa-plus"></i></a>
                </div>
                <div class="card-body table-responsive">
					<table class="table table-hover">
						<thead>
				    		<tr>
								<th width="5px">ID</th>
								<th>Disciplina</th>
								<th colspan="3">&nbsp;</th>
							</tr>
						</thead>
						<tbody>
							@foreach($disciplines as $discipline)
							<tr>
								<td>{{ $discipline->id }}</td>
								<td>{{ $discipline->disciplina }}</td>
							 
								<td width="10px">
									<a href="{{ route('disciplines.edit', $discipline) }}" type="button" class="btn btn-warning btn-sm">
										<i class="fas fa-edit"></i>
									</a>
								</td>
								<td width="10px">
									{!! Form::open(['route' => ['disciplines.destroy', $discipline->id],
									'method'=> 'DELETE']) !!}
										<button class="btn btn-sm btn-danger">
											<i class="fas fa-trash-alt"></i>
										</button>
									{!! Form::close() !!}
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
					{{ $disciplines->render() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection