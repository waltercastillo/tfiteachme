<div class="form-group">
	<strong>{{ Form::label('disciplina', 'Disciplina: ') }}</strong>
	{{ Form::text('disciplina',  null, ['class' => 'form-control', 'placeholder' => 'Disciplina...']) }}
</div>

<div class="form-group">
	{{ Form::submit('Guardar', ['class' => 'btn btn-primary btn-lg btn-block']) }}
</div>

<a href="{{ route('disciplines.index') }}" class="btn btn-outline-info mt-2"><i class="fas fa-long-arrow-alt-left"></i></a>
