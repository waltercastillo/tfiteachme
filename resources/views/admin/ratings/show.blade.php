@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-12 col-md-8">
            <div class="card">
                <div class="card-header  text-center">
                	<strong>Detalle:</strong>
                </div>
                <div class="card-body text-center">
					<p><strong>Profesor: </strong>{{$rating->user->name}} {{$rating->user->apellido}}</p>
					<hr>
					<p><strong>Puntos: </strong>{{$rating->puntos}}</p>
					<hr>
                    <p><strong>Comentario: </strong>{{$rating->comentario}}</p>
                    <hr>
					<p><strong>Fecha de creación: </strong>{{$rating->created_at->format('d/m/Y H:i:s')}}</p>
					<hr>
					<p><strong>Ultima actualización: </strong>{{$rating->updated_at->format('d/m/y h:i:s')}}</p>
					<hr>
                    <a href="{{ URL::previous() }}" class="btn btn-outline-info mt-2"><i class="fas fa-long-arrow-alt-left"></i></a>
                </div>
            </div>
            </div>
        </div>
    </div>
</div>

@endsection
