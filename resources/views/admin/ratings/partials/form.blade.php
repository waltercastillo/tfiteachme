<div class="form-group">
	<strong>{{ Form::label('puntos', 'Puntos: ') }}</strong>
	{!! Form::selectRange('puntos', 1, 5, $rating->puntos, [ 'class' => 'form-control', 'placeholder' => 'Puntos...']) !!}
{{-- 	{!! Form::select('puntos', [$rating->puntos,]) !!}
	{!!Form::select('puntos', [	'1', '2', '3', '4', '5'],'seleccione...')!!} --}}

</div>
<div class="form-group">
	<strong>{{ Form::label('comentario', 'Comentario: ') }}</strong>
	{{ Form::textarea('comentario',  null, ['class' => 'form-control', 'placeholder' => 'Comentario...', 'rows' => 2]) }}
</div>
<div class="form-group">
	{{ Form::submit('Guardar', ['class' => 'btn btn-primary btn-lg btn-block']) }}
</div>

<a href="{{ route('ratings.index') }}" class="btn btn-outline-info mt-2"><i class="fas fa-long-arrow-alt-left"></i></a>
