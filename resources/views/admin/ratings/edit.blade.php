@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-12 col-md-8">
            <div class="card">
                <div class="card-header">
                    <strong>Editar Puntuación :</strong>
                </div>
                <div class="card-body  text-center">
                    @include('error')
                    {!! Form::model($rating, ['route' => ['ratings.update', $rating], 
                    'method' => 'PUT']) !!}
                    @include('admin.ratings.partials.form')
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
