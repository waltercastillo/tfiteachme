@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-12 col-md-8">
            <div class="card">
                <div class="card-header">
                	<strong>Lista de ratings :</strong>
					{{-- <a href="{{ route('ratings.create') }}" type="button" class="btn btn-sm btn-success float-right" ><i class="fas fa-plus"></i></a> --}}
                </div>
                <div class="card-body table-responsive">
					<table class="table table-hover">
						<thead>
				    		<tr>
				    			
								<th width="5px">ID</th>
								<th>ID/Email/Profesor</th>
								<th>#</th>
								{{-- <th>Comen.</th> --}}
								<th colspan="3">&nbsp;</th>
							</tr>
						</thead>
						<tbody>
							@foreach($ratings as $rating)
							<tr>
								<td>{{ $rating->id }}</td>
								<td>{{ $rating->user->id }} -
									{{ $rating->user->email }} -
									{{ $rating->user->apellido }}
								</td>
								<td>{{ $rating->puntos }}</td>
								{{-- <td>{{ $rating->comentario }}</td> --}}
								<td width="10px">
									<a href="{{ route('ratings.show', $rating) }}" class="btn btn-primary btn-sm">
										<i class="fas fa-eye"></i>
									</a>
								</td>
								<td width="10px">
									<a href="{{ route('ratings.edit', $rating) }}" type="button" class="btn btn-warning btn-sm">
										<i class="fas fa-edit"></i>
									</a>
								</td>

								<td width="10px">
									{!! Form::open(['route' => ['ratings.destroy', $rating->id],
									'method'=> 'DELETE']) !!}
										<button class="btn btn-sm btn-danger">
											<i class="fas fa-trash-alt"></i>
										</button>
									{!! Form::close() !!}
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
					{{ $ratings->render() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection